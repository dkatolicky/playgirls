<?php

namespace UserUtils;

use Nette\Image;
use Nette;

/**
 * Description of PhotoUtils
 *
 * @author David
 */
class PhotoUtils {

	public static function getOrientation($name,$context = null){
		if (!is_null($name) && $name != "") {
			$name = self::separateFilePath($name);
			$file = $context->parameters['photoDir'] . '/' . $name->folder . $name->filename .'.'.$name->ext;
			$image = Image::fromFile($file);
			if($image->getWidth()>=$image->getHeight()){
				return 'horizontal';
			}else{
				return 'vertical';
			}
		}
	}
	/**
	 * Vrati obrazek o pozadovanych velikostech. Pokud neexistuje, vytvori se
	 *
	 * @param type $name
	 * @param type $sizeWidth
	 * @param type $sizeHeight
	 * @param type $sizeWidth2 - pokud neni null, pouzije se pro fotky nalezato
	 * @param type $sizeHeight2 - pokud neni null, pouzije se pro fotky nalezato
	 * 
	 * @return string
	 */
	public static function getPhoto($name = null, $sizeWidth, $sizeHeight, $flag = 'FIT', $context = null, $sizeWidth2 = null, $sizeHeight2 = null) {

		if (!is_null($name) && $name != "") {
			$name = self::separateFilePath($name);
			$file = $context->parameters['photoCacheDir'] . '/' . $name->folder . $name->filename . "-" . $sizeWidth . "-" . $sizeHeight . "-" . $flag . "." . $name->ext;


			if (!file_exists($file)) {
				if (is_dir($context->parameters['photoCacheDir'] . '/' . $name->folder) === FALSE) {
					mkdir($context->parameters['photoCacheDir'] . '/' . $name->folder);
				}
				$origFileName = "/" . $name->folder . $name->filename . "." . $name->ext;
				$origFile = $context->parameters['photoDir'] . $origFileName;

				/* pokud neexistuje soubor, stahne se z originalniho uloziste */
				if (!file_exists($origFile)) {
					file_put_contents($origFile, file_get_contents($context->parameters['originalFilesUrl'] . $origFileName));
				}
				/* end */

				if (file_exists($origFile)) {
					$_img = Image::fromFile($origFile);
//					dump($sizeWidth2,$sizeHeight2);exit;
					if ($sizeWidth2 !== null && $sizeHeight2 !== null) {
						$pomer = $_img->getWidth() / $_img->getHeight();
						if ($pomer > 1) {
							$sizeWidth = $sizeWidth2;
							$sizeHeight = $sizeHeight2;
							Nette\Diagnostics\Debugger::barDump($origFile);
						}
					}
					switch ($flag) {
						case 'FIT':
							$_img->resize($sizeWidth, $sizeHeight, Image::FIT);
							break;
						case 'FILL':
							$_img->resize($sizeWidth, $sizeHeight, Image::FILL);
							break;
						case 'STRETCH':
							$_img->resize($sizeWidth, $sizeHeight, Image::STRETCH);
							break;
						case 'EXACT':
							$_img->resize($sizeWidth, $sizeHeight, Image::EXACT);
							break;
					}
					$_img->sharpen();
					$_img->save($file);
				}
			}
			//oreze cestu k fotce pokud web neni v rootu apache
			if ($context->getService('httpRequest')->getUrl()->getScriptPath() != '/') {
				$x = str_replace($context->getService('httpRequest')->getUrl()->getScriptPath(), '', $context->parameters['wwwDir'] . '/');
//				dump('----',$x,'----');
			} else {
				$x = $context->parameters['wwwDir'];
			}
			return $context->getService('httpRequest')->getUrl()->getScheme().'://'.$context->getService('httpRequest')->getUrl()->getHost().str_replace($x, '', $file);
		} else {
			return "";
		}
	}

	/**
	 * rozparsuje cestu k souboru na folder, filename a priponu
	 * 
	 * @param string $name
	 * @return \Nette\ArrayHash
	 */
	private static function separateFilePath($name) {
		$return = new \Nette\ArrayHash();
		$name = explode('/', $name);
		$filename = $name[count($name) - 1];
		$_ext = explode(".", $filename);
		$return->ext = $_ext[count($_ext) - 1];
		unset($_ext[count($_ext) - 1]);
		$return->filename = implode('.', $_ext);
		$name[count($name) - 1] = "";
		$return->folder = implode('/', $name);
		unset($name);
		return $return;
	}

}
