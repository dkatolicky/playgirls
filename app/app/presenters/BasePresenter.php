<?php

namespace App\Presenters;

use Nette,
		App\Model;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {

	/**
	 * @var \App\dao\MenuDAO
	 */
	protected $menuDAO;

	/**
	 * @var \App\dao\SlecnaDAO
	 */
	protected $slecnaDAO;

	public function injectBasePresenter(\App\dao\MenuDAO $menuDAO, \App\dao\SlecnaDAO $slecnaDAO) {
		$this->menuDAO = $menuDAO;
		$this->slecnaDAO = $slecnaDAO;
	}

	function startup() {
		parent::startup();

		$this->template->setTranslator(new \App\Model\MyTranslator($this->getParam('lang')));
		$this->template->registerHelper('getPhoto', function($image, $width, $height, $flag) {
			return \UserUtils\PhotoUtils::getPhoto($image, $width, $height, $flag, $this->getPresenter()->context);
		});
		$this->template->registerHelper('fixSpace', function($text, $space = ' ') {
			return str_replace($space, '&nbsp;', $text);
		});


		$x = $this->getSession('confirm18');
		if (!isset($x->prihlasen) || $x->prihlasen !== true) {
			if (!($this->presenter instanceof ConfirmPresenter)) {

				$x->enterPage = $this->context->getService('httpRequest')->url->path;
				$this->redirect("Confirm:confirm");
			}
		}
		$this['topMenu'] = new \App\Components\TopMenu($this->menuDAO);
		$this['slecnydetail'] = new \App\Components\Slecnydetail();
		$this['slecny'] = new \App\Components\Slecny();
		$this['slecnyonline'] = new \App\Components\Slecnyonline();
		$this['slecnyoffline'] = new \App\Components\Slecnyoffline();
		$this['detail'] = new \App\Components\Detail();
		$this['detailPlaygirls'] = new \App\Components\DetailPlaygirls();
		$this['fullDetailPlaygirls'] = new \App\Components\FullDetailPlaygirls();
//		$this['kalendar'] = new \App\Components\Kalendar($this->slecnaDAO);
//		$this['kalendar2'] = new \App\Components\Kalendar2($this->slecnaDAO);
	}

	/**
	 * Metoda na ulozeni souboru do slozky photoDir
	 * 
	 * @param type $img
	 * @return string
	 * @throws Exception
	 */
	public function saveFile($img, $path) {
		if ($img->isOk() === true && $img->isImage() === true) {
			/* Pokud byl uploadnuty soubor v poradku a byl to obrazek, ulozi se do slozky photos */
			switch ($img->getContentType()) {
				case 'image/gif':
					$pripona = ".gif";
					break;
				case 'image/png':
					$pripona = ".png";
					break;
				case 'image/jpeg':
					$pripona = ".jpg";
					break;
			}
			$filename = md5($img->getName() . time()) . $pripona;
			$img->move($path . "/" . $filename);
			return $filename;
		} else {
			throw new Exception("Chyba při uploadu souboru", 500);
		}
	}

}
