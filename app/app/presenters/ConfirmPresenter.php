<?php

namespace App\Presenters;
use Nette\Application\UI\Form;
/**
 * Description of PagesPresenter
 *
 * @author David
 */
class ConfirmPresenter extends BasePresenter {

	public function renderConfirm() {
		
	}

	public function createComponentConfirmCs() {
		$form = new Form;
		$form->addSubmit('enter', 'Vstoupit');
		$form->addSubmit('leave', 'Odejít');
		$form->onSuccess[] = $this->confirmCsSucceded;
		return $form;
	}

	public function confirmCsSucceded(Form $form) {
		$vals = $form->getHttpData();
		if (isset($vals['enter'])) {
			$x = $this->getSession("confirm18");
			//$x->setExpiration(0);
			$x->prihlasen = true;
			if ($x->enterPage) {
				$enterpage = str_replace('en/', '', $x->enterPage);
				$this->redirectUrl($enterpage);
			} else {
				$this->redirect("Pages:default", array('lang' => "cs"));
			}
		} else {
			$this->redirectUrl('http://google.com');
		}
	}

	public function createComponentConfirmEn() {
		$form = new Form;
		$form->addSubmit('enter', 'Enter');
		$form->addSubmit('leave', 'Leave');
		$form->onSuccess[] = $this->confirmEnSucceded;
		return $form;
	}

	public function confirmEnSucceded(Form $form) {
		$vals = $form->getHttpData();
		if (isset($vals['enter'])) {
			$x = $this->getSession("confirm18");
			//$x->setExpiration("1 day");
			$x->prihlasen = true;
			if ($x->enterPage) {
				if ($this->context->getService('httpRequest')->url->scriptPath != '/') {
					$enterpage = str_replace($this->context->getService('httpRequest')->url->scriptPath, '', $x->enterPage);
				} else {
					$enterpage = $x->enterPage;
				}
				$enterpage = str_replace('en/', '', $enterpage);
				$enterpage = $this->context->getService('httpRequest')->url->scriptPath . 'en/' . $enterpage;
				$this->redirectUrl($enterpage);
			} else {
				$this->redirect("Pages:default", array('lang' => "en"));
			}
		} else {
			$this->redirectUrl('http://google.com');
		}
	}

}
