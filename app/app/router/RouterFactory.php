<?php

namespace App;

use Nette,
		Nette\Application\Routers\RouteList,
		Nette\Application\Routers\Route,
		Nette\Application\Routers\SimpleRouter;

/**
 * Router factory.
 */
class RouterFactory {

	/**
	 * @return \Nette\Application\IRouter
	 */
	public function createRouter() {
		$router = new RouteList();
		$router[] = new Route('index.php', 'Homepage:default', Route::ONE_WAY);


		$router[] = new Route('[<lang=cs cs|en>/]detail/<slecna>-<jmeno>', array(
				'presenter' => 'Pages',
				'action' => 'default',
				'slecna' => NULL,
				'item'=>'detail'
		));
//		$router[] = new Route('[<lang=cs cs|en>/]novinky/', array(
//				'presenter' => 'Pages',
//				'action' => 'novinky',
//		));
//		$router[] = new Route('[<lang=cs cs|en>/]photo/<name>/<suffix>', array(
//				'presenter' => 'Photo',
//				'action' => 'get',
//				'name' => NULL,
//				'suffix' => NULL
//		));

//    $router[] = new Route('[<lang=cs cs|en>/]kalendar/', array(
//                'presenter' => 'Pages',
//                'action' => 'kalendar',
//            ));
		$router[] = new Route('[<lang=cs cs|en>/]confirm/', array(
				'presenter' => 'Confirm',
				'action' => 'confirm',
		));

		$router[] = new Route('[<lang=cs cs|en>/]<item=homepage>[/<slecna>]', array(
				'presenter' => 'Pages',
				'action' => 'default',
				'item' => NULL,
		));

		return $router;
	}

}
