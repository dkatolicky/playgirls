<?php

namespace App\dao;

/**
 * Description of NovinkyDAO
 *
 * @author David
 */
class PriceDAO extends BaseDAO {

	public $table = 'price';
	public $model = 'App\Model\Price';

	public function save(\App\Model\Price $model) {
		$data = array(
				'id' => $model->id,
				'slecna_id' => is_object($model->slecna)?$model->slecna->id:null,
				'podnik_id' => is_object($model->podnik)?$model->podnik->id:null,
				'col0' => $model->col0,
				'col1' => $model->col1,
				'col2' => $model->col2,
				'col3' => $model->col3,
				'col4' => $model->col4,
				'col5' => $model->col5,
				'col6' => $model->col6,
				'col7' => $model->col7,
				'col8' => $model->col8,
				'currency' => $model->currency,
				'type_id' => $model->type_id,
		);
		return parent::saveData($data, $model);
	}

	public function getByPodnik(\App\Model\Podnik $podnik, $type) {
		$p = $this->db->table($this->table)->where('podnik_id', $podnik->id)->where('type_id', $type);
		if ($p->count() > 0) {
			$price = new $this->model($p->fetch()->toArray());
		} else {
			$price = new $this->model(array('currency'=>'CZK','type_id'=>$type,'podnik_id'=>$podnik->id));
		}
		$price->setSlecna(null);
		$price->setPodnik($podnik);
		return $price;
	}
	public function getBySlecna(\App\Model\Slecna $slecna, $type) {
		$p = $this->db->table($this->table)->where('slecna_id', $slecna->id)->where('type_id', $type);
		if ($p->count() > 0) {
			$price = new $this->model($p->fetch()->toArray());
		} else {
			$price = new $this->model(array('currency'=>'CZK','type_id'=>$type,'slecna_id'=>$slecna->id));
		}
		$price->setPodnik(null);
		$price->setSlecna($slecna);
		return $price;
	}
	
	public function deleteForSlecna(\App\Model\Slecna $slecna){
		$this->db->table($this->table)->where('slecna_id',$slecna->id)->delete();
	}

}
