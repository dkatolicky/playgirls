<?php

namespace App\dao;

/**
 * Description of MenuDAO
 *
 * @author David
 */
class MenuDAO extends BaseDAO {

	public $table = 'menu';
	public $model = 'App\Model\Menu';

	public function getAll() {
		$menus = parent::getAll();
		$pages = $this->content->pageDAO->getAll();
		foreach ($menus as $key => $values) {
			$values->setPage($pages[$values->getPage_id()]);
		}
		unset($pages);
		return $menus;
	}

	public function getAllByWebId($webid) {
		$menus = array();
		$pages = $this->content->pageDAO->getAll();
		foreach ($this->db->table($this->table)->where('web_id', $webid)->order('ord') as $row) {
			$menus[$row->id] = new $this->model($row->toArray());
			$menus[$row->id]->setPage($pages[$menus[$row->id]->getPage_id()]);
		}

		unset($pages);
		return $menus;
	}

	public function getByWebIdAndItem($webid, $item) {
		$menus = array();
		$pages = $this->content->pageDAO->getAll();
		$menu = null;
		foreach ($this->db->table($this->table)->where(array('web_id' => $webid, 'item' => $item)) as $row) {
			$menu = new $this->model($row->toArray());
			$menu->setPage($pages[$menu->getPage_id()]);
		}

		unset($pages);
		return $menu;
	}

	public function get($id = null, array $where = null) {
		$menu = parent::get($id, $where);
		$menu->setPage($this->content->pageDAO->get($menu->getPage_id()));
		return $menu;
	}

	public function save(\App\Model\Menu $model) {
		$data = array(
				'item' => $model->item,
				'name' => $model->name,
				'name_en' => $model->name_en,
				'lang' => $model->lang,
				'page_id' => $model->page_id,
				'visible' => $model->visible,
				'ord' => $model->ord,
				'web_id' => $model->web_id,
		);
		$menu = parent::saveData($data, $model);
		$page = $this->content->pageDAO->save($model->page);
		$menu->setPage($page);
		return $menu;
	}

}
