<?php

namespace App\dao;

/**
 * Description of NovinkyDAO
 *
 * @author David
 */
class SlecnaDAO extends BaseDAO {

	public $table = 'slecna';
	public $model = 'App\Model\Slecna';
	public $slecna = array();

	public function getAll() {
		$girls = array();
		foreach ($this->db->table($this->table)->order('ord') as $row) {
			$girls[$row->id] = new $this->model($row->toArray());
		}
		
		foreach ($girls as $key => $value) {
			if (!$this->exist($key)) {
				$girls[$key] = $this->loadEntities($value);
				$this->{$this->table}[$key] = $girls[$key];
			} else {
				$girls[$key] = $this->{$this->table}[$key];
			}
		}
		return $girls;
	}

	public function getAllActive() {
		$girls = parent::getAllActive();
		foreach ($girls as $key => $value) {
			if (!$this->exist($key)) {
				$girls[$key] = $this->loadEntities($value);
				$this->{$this->table}[$key] = $girls[$key];
			} else {
				$girls[$key] = $this->{$this->table}[$key];
			}
		}
		return $girls;
	}

	public function getAllActiveByPodnikIds(array $podnikIds) {
		$girls = array();
		foreach ($this->db->table($this->table)->where('active', true)->where('podnik_id', $podnikIds)->order('ord') as $row) {
			if (!$this->exist($row->id)) {
				$girls[$row->id] = new $this->model($row->toArray());
				$girls[$row->id] = $this->loadEntities($girls[$row->id]);
				$this->{$this->table}[$row->id] = $girls[$row->id];
			} else {
				$girls[$row->id] = $this->{$this->table}[$row->id];
			}
		}
		return $girls;
	}

	public function get($id = null, array $where = null) {
		if (!$this->exist($id)) {
			$girl = parent::get($id, $where);
			$girl = $this->loadEntities($girl);
			$this->{$this->table}[$id] = $girl;
		}
		return $this->{$this->table}[$id];
	}

	/**
	 * nahraje do modelu ostatni entity
	 * @param \App\Model\Slecna $slecna
	 * @return \App\Model\Slecna
	 */
	private function loadEntities(\App\Model\Slecna $slecna) {
		$slecna->addLang('cs', $this->content->slecnaLangDAO->getGirlLang('cs', $slecna->getId()));
		$slecna->addLang('en', $this->content->slecnaLangDAO->getGirlLang('en', $slecna->getId()));

		
		$slecna->setPhotos($this->content->slecnaPhotoDAO->getAllByGirlId($slecna->getId()));
		$slecna->setPodnik($this->content->podnikDAO->get($slecna->getPodnik_id()));
//				$slecna->setServiceTypes($this->content->serviceTypeDAO->getAll());
$slecna->setServiceTypes($slecna->podnik->serviceType);
		$slecna->setPrice_privat($this->content->priceDAO->getBySlecna($slecna, 1));
		$slecna->setPrice_escort_in_prague($this->content->priceDAO->getBySlecna($slecna, 2));
		$slecna->setPrice_escort_out_prague($this->content->priceDAO->getBySlecna($slecna, 3));
		$slecna->setPrice_escort_out_prague_more31($this->content->priceDAO->getBySlecna($slecna, 4));

		$services = $this->getGirlOfferedServices($slecna);

		foreach ($slecna->serviceTypes as $typeKey => $typeValue) {
			foreach ($typeValue->services as $serviceKey => $serviceValue) {
				if (in_array($serviceKey, $services)) {
					$serviceValue->setOffered(true);
				}
			}
		}
		return $slecna;
	}

	public function getByUrlOrIdAndPodnikIds($urlOrId, array $podnikIds) {
		if (is_numeric($urlOrId)) {
			$id = $this->db->table($this->table)->where(array('id' => $urlOrId, 'active' => true, 'podnik_id' => $podnikIds))->select('id')->limit(1)->fetch()->id;
		} else {
			$id = $this->db->table($this->table)->where(array('url' => $urlOrId, 'active' => true, 'podnik_id' => $podnikIds))->select('id')->limit(1)->fetch()->id;
		}
		return $this->get($id);
	}

	/**
	 * vrati pole IDcek poskytovanych sluzeb danou slecnou
	 * @param int $slecna_id
	 * @return array
	 */
	public function getGirlOfferedServices(\App\Model\Slecna $slecna) {
		$services = array();

		foreach ($this->db->table('slecnaservice')->where(array('slecna_id'=>$slecna->id,'service.servicetype_id'=>array_keys($slecna->serviceTypes))) as $row) {
			$services[] = $row->service_id;
		}
		return $services;
	}

	public function save(\App\Model\Slecna $model) {
		$data = array(
				'url' => $model->getUrl(),
				'mainfoto' => $model->getMainfoto(),
				'mainfotoland' => $model->getMainfotoland(),
				'active' => $model->getActive(),
				'ord' => $model->getOrd(),
				'podnik_id' => $model->getPodnik_id(),
				'specialprice' => $model->getSpecialprice(),
				'multiprofil' => $model->getMultiprofil(),
				'multi1' => $model->getMulti1(),
				'multi2' => $model->getMulti2(),
				'multi3' => $model->getMulti3(),
				'multi4' => $model->getMulti4(),
				'photo1' => $model->getPhoto1(),
				'photo2' => $model->getPhoto2(),
				'photo3' => $model->getPhoto3(),
				'photo4' => $model->getPhoto4(),
				'photo5' => $model->getPhoto5(),
		);

		$model = parent::saveData($data, $model);
		/* ulozeni jazykovych mutaci */

		foreach ($model->langs as $lang) {
			$lang->setSlecna_id($model->id);
			$this->content->slecnaLangDAO->save($lang);
		}
		/* ulozeni cen */
		if ($model->specialprice == 1) {
			$this->content->priceDAO->save($model->price_privat);
			$this->content->priceDAO->save($model->price_escort_in_prague);
			$this->content->priceDAO->save($model->price_escort_out_prague);
			$this->content->priceDAO->save($model->price_escort_out_prague_more31);
		} else {
			$this->content->priceDAO->deleteForSlecna($model);
		}
		/* ulozeni sluzeb co nabizi */
		$this->saveOfferedServices($model);

		return $model;
	}

	private function saveOfferedServices(\App\Model\Slecna $model) {
		$insert = array();
		$this->db->table('slecnaservice')->where('slecna_id', $model->id)->delete();

		foreach ($model->serviceTypes as $serviceType) {
			foreach ($serviceType->services as $service) {
				if ($service->getOffered()) {
					$insert[] = array('slecna_id' => $model->id, 'service_id' => $service->id);
				}
			}
		}
		if (count($insert) > 0) {
			$this->db->table('slecnaservice')->insert($insert);
		}
	}

	public function savePhotoPositionByArray($id, $tosave) {
		$original = array();
		foreach ($this->db->table('slecnaphoto')->where('slecna_id', $id)->order('ord') as $_photo) {
			$original[$_photo->id] = $_photo->ord;
		}
		foreach ($this->compareTwoArrayValues($tosave, $original) as $idphoto) {
			$this->db->table('slecnaphoto')->where(array('id' => $idphoto, 'slecna_id' => $id))->update(array('ord' => $tosave[$idphoto]));
		}
	}

	public function compareTwoArrayValues(array $array1, array $array2) {
		$diff = array();
		foreach ($array2 as $id => $ord) {
			if ($array1[$id] != $ord) {
				$diff[] = $id;
			}
		}
		return $diff;
	}

	/**
	 * vytvori duplikat profilu
	 */
	public function createDuplicate($slecna_id) {
		$slecna = $this->get($slecna_id);
		$slecna->id = null;
		$slecna->url.= '-kopie';
		$slecna->photos = array();
		$slecna->specialprice = false;
		foreach ($slecna->langs as $lang) {
			$lang->id = null;
		}
		$slecna = $this->save($slecna);
		return $slecna;
	}

	public function getAllByPrivate() {
		$return = array();
		$podniky = new PodnikDAO($this->db, $this->content);
		$podniky = $podniky->getAllFetchPairs('id', 'name');
		foreach ($podniky as $id => $podnik) {
			$return[$id] = array(
					'name' => $podnik,
					'podnik_id'=>$id,
					'slecny' => array()
			);
		}

		foreach ($this->getAll() as $slecna) {
			$return[$slecna->getPodnik_id()]['slecny'][] = $slecna;
		}

		return $return;
	}

	public function setLast($slecna_id) {
		$slecna = $this->get($slecna_id);
		$maxOrd = $this->db->table($this->table)->max('ord');
		$slecna->ord = $maxOrd + 10;
		$slecna = $this->save($slecna);
		return $slecna;
	}

}
