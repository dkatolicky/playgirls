<?php

namespace App\dao;

/**
 * Description of NovinkyDAO
 *
 * @author David
 */
class WebDAO extends BaseDAO {

	public $table = 'web';
	public $model = 'App\Model\Web';

	public function save(\App\Model\Web $model) {
		$data = array(
			'name' => $model->name,
			'url' => $model->url,
		);
		return parent::saveData($data, $model);
	}

}
