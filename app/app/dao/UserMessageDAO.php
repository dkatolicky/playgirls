<?php

namespace App\dao;

use Nette;
use Nette\Object;

/**
 * Description of UserMessageDAO
 *
 * @author David
 */
class UserMessageDAO extends BaseDAO {

	/**
	 * vrati vsechny zpravy pro konkretniho uzivatele
	 * 
	 * @param int $userId
	 * @return array of \App\model\Message
	 */
//	public function getAll($userId) {
//		return $this->getMessages($userId, null);
//	}

	/**
	 * vrati urcity pocet zprav pro konkretniho uzivatele
	 * 
	 * @param int $userId
	 * @param int $count pocet vracenych zprav
	 * @return array of \App\model\Message
	 */
	public function getMessages($userId, $count = 3) {
		$ret = array();
		foreach ($this->db->table('user_message')->where('user_id', $userId)->order('date DESC')->limit($count) as $message) {
			$x = new \App\model\Message;
			$x->id = $message->id;
			$x->readed = $message->readed;
			$x->date = $message->date;
			$x->from = "todo";
			$x->subject = $message->subject;
			$x->text = $message->text;
			$ret[] = $x;
			unset($x);
		}
		return $ret;
	}

}
