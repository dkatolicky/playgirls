<?php

namespace App\dao;

/**
 * Description of NovinkyDAO
 *
 * @author David
 */
class NovinkyDAO extends BaseDAO {

	public $table = 'news';
	public $model = 'App\Model\News';

	public function save(\App\Model\News $model) {
		$data = array(
				'time' => $model->time,
				'description' => $model->description,
				'text' => $model->text,
				'description_en' => $model->description_en,
				'text_en' => $model->text_en,
				'active' => $model->active,
				'ord' => $model->ord,
				'lang' => $model->lang,
				'web_id' => $model->web_id
		);
		return parent::saveData($data, $model);
	}
	
		public function getAllByWebId($webid) {
		$news = array();
		foreach ($this->db->table($this->table)->where('web_id', $webid)->order('active,ord') as $row) {
			$news[$row->id] = new $this->model($row->toArray());
		}
		return $news;
	}
		public function getAllActiveByWebId($webid,$limit=10) {
		$news = array();
		foreach ($this->db->table($this->table)->where('web_id', $webid)->where('active',1)->order('active,ord')->limit($limit) as $row) {
			$news[$row->id] = new $this->model($row->toArray());
		}
		return $news;
	}

}
