<?php

namespace App\dao;

/**
 * Description of NovinkyDAO
 *
 * @author David
 */
class SlecnaLangDAO extends BaseDAO {

	public $table = 'slecna_lang';
	public $model = 'App\Model\SlecnaLang';

	public function getGirlLang($lang, $slecna_id) {
		return new $this->model($this->db->table($this->table)->where('slecna_id', $slecna_id)->where('lang', $lang)->fetch()->toArray());
	}

	public function save(\App\Model\SlecnaLang $model) {
		$data = array(
				'slecna_id' => $model->getSlecna_id(),
				'lang' => $model->getLang(),
				'nazev' => $model->getNazev(),
				'jmeno' => $model->getJmeno(),
				'vl_jmeno' => $model->getVl_jmeno(),
				'vl_tel' => $model->getVl_tel(),
				'vl_email' => $model->getVl_email(),
				'vek' => $model->getVek(),
				'vyska' => $model->getVyska(),
				'vaha' => $model->getVaha(),
				'prsa' => $model->getPrsa(),
				'vlasy' => $model->getVlasy(),
				'oci' => $model->getOci(),
				'ochlupeni' => $model->getOchlupeni(),
				'jazyky' => $model->getJazyky(),
				'region' => $model->getRegion(),
				'ulice' => $model->getUlice(),
				'mesto' => $model->getMesto(),
				'metro' => $model->getMetro(),
				'telefon' => $model->getTelefon(),
				'email' => $model->getEmail(),
				'ostatni' => $model->getOstatni(),
				'ostatni_box' => $model->getOstatni_box(),
				'header' => $model->getHeader(),
				'footer' => $model->getFooter(),
				'ostatni_box_price_1' => $model->getOstatni_box_price_1(),
				'ostatni_box_price_2' => $model->getOstatni_box_price_2(),
		);

		return parent::saveData($data, $model);
	}

}
