<?php

namespace App\Components;

use Nette;
use Nette\Application\UI\Control;

/**
 * Description of leftMenu
 *
 * @author David
 */
class TopMenu extends Control {

	private $menuDAO;

	public function __construct(\App\dao\MenuDAO $menuDAO) {
		$this->menuDAO = $menuDAO;
	}

	public function render() {
		$template = $this->template;
		$this->template->setFile($this->getPresenter()->context->parameters['appDir'] . '/templates/components/topMenu.latte');
		$this->template->topmenu = $this->menuDAO->getAllByWebId($this->getPresenter()->context->parameters['webid']);
		if ($this->presenter->name != 'Confirm') {
			$template->render();
		}
	}

}
