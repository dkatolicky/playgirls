<?php

namespace App\Components;

use Nette\Application\UI\Control;

/**
 * Description of BaseComponent
 *
 * @author David
 */
class BaseComponent extends Control {

	public function render() {
		$this->template->setTranslator(new \App\Model\MyTranslator($this->getPresenter()->getParameter('lang')));
		$this->template->registerHelper('getPhoto', function($image, $width, $height, $flag, $width2 = null, $height2 = null) {
			return \UserUtils\PhotoUtils::getPhoto($image, $width, $height, $flag, $this->getPresenter()->context, $width2, $height2);
		});
		$this->template->registerHelper('separeMail', function($mail) {
			if (isset($mail) && $mail != '') {
				$mail = explode('@', $mail);
				switch ($mail[1]) {
					case 'seznam.cz':
						return $mail[0] . '<img class = "mail" src = "img/seznamcz.gif">';
						break;
					case 'escort69.cz':
						return $mail[0] . '<img class = "mail" src = "img/Escort69cz.gif">';
						break;
					default:
						return $mail;
						break;
				}
			} else {
				return $mail;
			}
		});
		$this->template->registerHelper('czechDay', function($dateN) {
			switch ($dateN) {
				case 1: $n = "Po ";
					break;
				case 2: $n = "Út ";
					break;
				case 3: $n = "st ";
					break;
				case 4: $n = "Čt ";
					break;
				case 5: $n = "Pá ";
					break;
				case 6: $n = "So ";
					break;
				case 7: $n = "Ne ";
					break;
			}
			return $n;
		});
	}

}
