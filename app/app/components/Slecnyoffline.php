<?php

namespace App\Components;

use Nette;

/**
 * Description of leftMenu
 *
 * @author David
 */
class Slecnyoffline extends BaseComponent {

	public function render() {
		parent::render();
		$template = $this->template;
		$this->template->setFile($this->getPresenter()->context->parameters['appDir'] . '/templates/components/slecnyoffline.latte');
		$this->template->lang = $this->getPresenter()->getParameter('lang');
		$this->template->slecny = $this->getPresenter()->context->getService('slecnaDAO')->getAllActiveByPodnikIds($this->getPresenter()->context->parameters['podnikid']);
		$date = new Nette\DateTime;
		$date->modify('-4 hours');
		$date->setTime(0, 0, 0);
		$this->template->calendar = $this->getPresenter()->context->getService('calendarDAO')->getGirlsCalendarForDay($date);
		$template->render();
	}

}
