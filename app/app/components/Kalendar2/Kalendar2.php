<?php

namespace App\Components;

use Nette\Application\UI\Control;
use Model;

/**
 * Komponenta Kalendar2
 *
 * @author Anapajo
 */
class Kalendar2 extends BaseComponent {

	/**
	 * @var Model\Calendar
	 */
	private $modelCalendar;

	/**
	 * @var Model\Slecna
	 */
	private $modelSlecna;

	/**
	 * @var Model\Page
	 */
	private $modelPage;
	private $novinkyDAO;

	public function __construct(\App\dao\SlecnaDAO $slecna, \App\dao\CalendarDAO $calendar, \App\dao\PageDAO $page, \App\dao\NovinkyDAO $novinkyDAO) {
		$this->modelCalendar = $calendar;
		$this->modelSlecna = $slecna;
		$this->modelPage = $page;
		$this->novinkyDAO = $novinkyDAO;
	}

	public function render() {
		parent::render();
		$this->template->setTranslator(new \App\Model\MyTranslator($this->presenter->getParam('lang')));

		$pocetdnu = 17;
		$this->template->page = $this->modelPage;
		$this->template->pocetdnu = $pocetdnu;
		$this->template->modelSlecna = $this->modelSlecna;
		$this->template->modelCalendar = $this->modelCalendar;
		$this->template->news = $this->novinkyDAO->getAllActiveByWebId($this->presenter->context->parameters['webid'], 30);
		$dnes = false;
		$zitra = false;
		$headerColDate = array();
		foreach ($this->modelCalendar->createCalendar($pocetdnu) as $den) {
			$n = $this->modelCalendar->timestampToDay($den);
			if (!$dnes && (date('N', $den) == date('N'))) {
				$n = "Dnes";
				$dnes = true;
			} elseif ($dnes && !$zitra) {
				$n = "Zítra";
				$zitra = true;
			}
			$headerColDate[$den] = $n;
		}
		$this->template->headerColDate = $headerColDate;
		$this->template->setFile(__DIR__ . '/kalendar2.latte');
		return $this->template->render();
	}

}

?>
