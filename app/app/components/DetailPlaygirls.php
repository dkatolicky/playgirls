<?php

namespace App\Components;

use Nette;

/**
 * Description of Detail
 *
 * @author David
 */
class DetailPlaygirls extends BaseComponent {

	public function render() {

		parent::render();
		$template = $this->template;
		$this->template->setFile($this->getPresenter()->context->parameters['appDir'] . '/templates/components/detailPlaygirls.latte');
		$this->template->lang = $this->getPresenter()->getParameter('lang');
		$this->template->slecna = $this->getPresenter()->context->getService('slecnaDAO')->getByUrlOrIdAndPodnikIds($this->getPresenter()->getParameter('slecna'),$this->getPresenter()->context->parameters['podnikid']);
//		\Nette\Diagnostics\Debugger::$maxDepth = 5;
//		dump($this->template->slecna);
//		exit;
		$this->template->calendar = $this->getPresenter()->context->getService('calendarDAO')->getGirlCalendar2($this->getPresenter()->getParameter('slecna'),5);
		$template->render();
	}

}
