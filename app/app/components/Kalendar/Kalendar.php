<?php

namespace App\Components;

use Nette\Application\UI\Control;
use Model;

/**
 * Komponenta Kalendar
 *
 * @author Anapajo
 */
class Kalendar extends BaseComponent {

	/**
	 * @var Model\Calendar
	 */
	private $caledarDAO;

	/**
	 * @var Model\Slecna
	 */
	private $slecnaDAO;

	/**
	 * @var Model\Photo
	 */
	private $pageDAO;
	private $novinkyDAO;

	public function __construct(\App\dao\SlecnaDAO $slecna, \App\dao\CalendarDAO $calendar, \App\dao\PageDAO $page,  \App\dao\NovinkyDAO $novinkyDAO) {
		$this->caledarDAO = $calendar;
		$this->slecnaDAO = $slecna;
		$this->pageDAO = $page;
		$this->novinkyDAO= $novinkyDAO;
	}

	public function render() {
		parent::render();
		$this->template->setTranslator(new \App\Model\MyTranslator($this->presenter->getParam('lang')));

		$pocetdnu = 10;
		$this->template->page = $this->pageDAO;
		$this->template->pocetdnu = $pocetdnu;
		$this->template->slecnaDAO = $this->slecnaDAO;
		$this->template->modelCalendar = $this->caledarDAO;
		$this->template->news = $this->novinkyDAO->getAllActiveByWebId($this->presenter->context->parameters['webid'],30);
		$headerColDate = array();
		$dnes = false;
		$zitra = false;
		foreach ($this->caledarDAO->createCalendar($pocetdnu) as $den) {
			$n = $this->caledarDAO->timestampToDay($den);
			if(!$dnes && (date('N', $den) == date('N'))){
				$n = "Dnes";
				$dnes = true;
			}elseif($dnes && !$zitra){
				$n = "Zítra";
				$zitra = true;
			}
			$headerColDate[$den] = $n;
		}
		$this->template->headerColDate = $headerColDate;
		$this->template->setFile(__DIR__ . '/kalendar.latte');
		return $this->template->render();
	}

}

