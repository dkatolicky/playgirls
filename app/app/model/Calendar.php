<?php

namespace App\Model;

/**
 * Model pro kalendar ku slecne
 *
 * @author Pali Bazant <pali.bazant@gmail.com>
 */
class Calendar extends BaseModel {

	private $rows = array();
	private $popisky = array(
			0 => 'Celý den online',
			//1 => 'Na dotaz Zavoláním',
			//2 => 'Na dotaz přes SMS',
			3 => 'Může od',
			4 => 'Může do',
			5 => 'Čas dojezdu ke kli.',
			6 => 'Čas dojezdu do Prahy',
			7 => 'Dívka je daný den v',
			8 => 'Poznamka',
			9 => 'Poznámka'
	);
	private $timestamps = array();
	private $methodsNames = array(
			0 => 'online',
			1 => 'na_dotaz_zavolanim',
			2 => 'na_dotaz_pres_sms',
			3 => 'muze_od',
			4 => 'muze_do',
			5 => 'cas_dojezdu_ke_klientovi_note',
			6 => 'cas_dojezdu_do_prahy_note',
			7 => 'divka_je_v_dany_den_v',
			8 => 'cely_den_online_note',
			9 => 'note'
	);

	public function getMethodsNames() {
		return $this->methodsNames;
	}

	public function setMethodsNames($methodsNames) {
		$this->methodsNames = $methodsNames;
	}

	public function getTimestamps() {
		return $this->timestamps;
	}

	public function setTimestamps($timestamps) {
		if (!isset($this->timestamps[$timestamps])) {
			$this->timestamps[$timestamps] = $timestamps;
		}
	}

	public function getRows() {
		return $this->rows;
	}

	public function setRows($row, $timestamp) {
		$this->setTimestamps($timestamp);
		$this->rows[$timestamp] = $row;
	}

	public function getPopisky() {
		return $this->popisky;
	}

	public function getPopisek($id) {
		return $this->popisky[$id];
	}

	public function setPopisky($popisky) {
		$this->popisky = $popisky;
	}

	public function getCountPopisky() {
		return count($this->popisky);
	}

	public function isSetRow($timestamp) {
		return isset($this->rows[$timestamp]);
	}

	public function getRow($timestamp) {
		if ($this->isSetRow($timestamp)) {
			return $this->rows[$timestamp];
		} else {
			return new CalendarRow();
		}
	}

	public function writeValue($id) {
		return isset($this->popisky[$id]);
	}

	public function unsetValues() {
		$issetCelyDenOnlineNote = FALSE;
		$issetNaDotazZavolam = FALSE;
		$issetNaDotazSMS = FALSE;
		$issetMuzeOd = FALSE;
		$issetMuzeDo = FALSE;
		$issetCasDojezduKeKlientovy = FALSE;
		$issetCasDojezduDoPrahy = FALSE;
		$issetDivkaJeDanyDenV = FALSE;
		$issetPoznamka = FALSE;

		foreach ($this->rows as $row) {
			if (!is_null($row->getCely_den_online_note()) && !$issetCelyDenOnlineNote) {
				$issetCelyDenOnlineNote = TRUE;
			}
			if ($row->getNa_dotaz_zavolanim() && !$issetNaDotazZavolam) {
				$issetNaDotazZavolam = TRUE;
			}
			if ($row->getNa_dotaz_pres_sms() && !$issetNaDotazSMS) {
				$issetNaDotazSMS = TRUE;
			}
			if (!is_null($row->getMuze_od()) && !$issetMuzeOd) {
				$issetMuzeOd = TRUE;
			}
			if (!is_null($row->getMuze_do()) && !$issetMuzeDo) {
				$issetMuzeDo = TRUE;
			}
			if (!is_null($row->getCas_dojezdu_ke_klientovi_note()) && !$issetCasDojezduKeKlientovy) {
				$issetCasDojezduKeKlientovy = TRUE;
			}
			if (!is_null($row->getCas_dojezdu_do_prahy_note()) && !$issetCasDojezduDoPrahy) {
				$issetCasDojezduDoPrahy = TRUE;
			}
			if (!is_null($row->getDivka_je_v_dany_den_v()) && !$issetDivkaJeDanyDenV) {
				$issetDivkaJeDanyDenV = TRUE;
			}
			if (!is_null($row->getNote()) && !$issetPoznamka) {
				$issetPoznamka = TRUE;
			}
		}

		if (!$issetNaDotazZavolam) {
			unset($this->popisky[1]);
		}

		if (!$issetNaDotazSMS) {
			unset($this->popisky[2]);
		}
		if (!$issetMuzeOd) {
			unset($this->popisky[3]);
		}

		if (!$issetMuzeDo) {
			unset($this->popisky[4]);
		}

		if (!$issetCasDojezduKeKlientovy) {
			unset($this->popisky[5]);
		}

		if (!$issetCasDojezduDoPrahy) {
			unset($this->popisky[6]);
		}

		if (!$issetDivkaJeDanyDenV) {
			unset($this->popisky[7]);
		}

		if (!$issetCelyDenOnlineNote) {
			unset($this->popisky[8]);
		}

		if (!$issetPoznamka) {
			unset($this->popisky[9]);
		}
	}

	public function getRowByNameAndTimestamp($name, $timestamp) {
		if ($this->isSetRow($timestamp)) {
			//$getter = 'get' . \Nette\Utils\Strings::firstUpper($name) . '()';
//			dump($getter);exit;
			return $this->rows[$timestamp]->callName($name);
		} else {
			return NULL;
		}
	}

}
