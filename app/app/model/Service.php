<?php

namespace App\Model;

/**
 * Description of Slecna
 *
 * @author David
 */
class Service extends BaseModel {

	private $id;
	private $description;
	private $description_en;
	private $servicetype_id;
	private $ord;

	/**
	 * nabizi ji slecna?
	 * @var type 
	 */
	private $offered = false;

	public function getId() {
		return $this->id;
	}

	public function getDescription() {
		return $this->description;
	}

	public function getDescription_en() {
		return $this->description_en;
	}

	public function getServicetype_id() {
		return $this->servicetype_id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function setDescription_en($description_en) {
		$this->description_en = $description_en;
	}

	public function setServicetype_id($servicetype_id) {
		$this->servicetype_id = $servicetype_id;
	}

	public function getOffered() {
		return $this->offered;
	}

	public function setOffered($offered) {
		$this->offered = $offered;
	}

	public function getOrd() {
		return $this->ord;
	}

	public function setOrd($ord) {
		$this->ord = $ord;
	}

}
