<?php

namespace App\Model;

use Nette;
use Nette\Object;

/**
 * Description of BaseModel
 *
 * @author David
 */
class BaseModel extends Object {

	public function __construct($page = array()) {
		$this->cast($page);
	}

	public function cast($model) {
		if (isset($model->id)) {
			$this->setId($model->id);
		}
		foreach ($model as $key => $value) {
			$setter = 'set' . ucfirst($key);
			$this->$setter($value);
		}
	}

}
