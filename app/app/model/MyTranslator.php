<?php
namespace App\Model;

use Nette\Utils\Strings;

/**
 * Prekladani do EN
 *
 * @author Anapajo
 */
class MyTranslator implements \Nette\Localization\ITranslator {

  private $lang;
  private $translateFile;
  private $translation = array();

  const FILE = "locale.en";

  public function __construct($lang) {
    $this->lang = $lang;
    $this->translateFile = file_get_contents(__DIR__ . "/" . self::FILE);
    foreach (explode("\n", $this->translateFile) as $row) {
      $_r = explode("|", $row);
      if(is_array($_r) && isset($_r[0])){
        $ret = "";
        if(!isset($_r[1]) || $_r[1] == ""){
          $ret = $_r[0];
        }  else {
          $ret = $_r[1];
        }
      $this->translation[Strings::lower($_r[0])] = Strings::normalize($ret);
      }
    }
  }

  /**
   * Translates the given string.
   * @param  string   message
   * @param  int      plural count
   * @return string
   */
  public function translate($message, $count = NULL) {
    $_mess = Strings::lower($message);
    if (array_key_exists($_mess, $this->translation)) {
      if ($this->lang == "en") {
        return ($this->translation[$_mess] == "") ? $message : $this->translation[$_mess];
      } else {
        return $message;
      }
    } else {
      file_put_contents(__DIR__ . "/" . self::FILE, "\n" . $message . "|", FILE_APPEND);
      return $message;
    }
  }

}
