<?php

namespace App\Model;

/**
 * Description of Menu
 *
 * @author David
 */
class Podnik extends BaseModel {

	private $id;
	private $active;
	private $name;
	private $name_en;
	private $ceny_popis;
	private $ceny_popis_en;
	private $ceny_popis2;
	private $ceny_popis2_en;
	private $course;
	private $price_privat;
	private $price_escort_in_prague;
	private $price_escort_out_prague;
	private $price_escort_out_prague_more31;
	private $serviceType;

	public function getId() {
		return $this->id;
	}

	public function getName() {
		return $this->name;
	}

	public function getName_en() {
		return $this->name_en;
	}

	public function getCeny_popis() {
		return $this->ceny_popis;
	}

	public function getCeny_popis_en() {
		return $this->ceny_popis_en;
	}

	public function getCeny_popis2() {
		return $this->ceny_popis2;
	}

	public function getCeny_popis2_en() {
		return $this->ceny_popis2_en;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function setName_en($name_en) {
		$this->name_en = $name_en;
	}

	public function setCeny_popis($ceny_popis) {
		$this->ceny_popis = $ceny_popis;
	}

	public function setCeny_popis_en($ceny_popis_en) {
		$this->ceny_popis_en = $ceny_popis_en;
	}

	public function setCeny_popis2($ceny_popis2) {
		$this->ceny_popis2 = $ceny_popis2;
	}

	public function setCeny_popis2_en($ceny_popis2_en) {
		$this->ceny_popis2_en = $ceny_popis2_en;
	}

	public function getActive() {
		return $this->active;
	}

	public function setActive($active) {
		$this->active = $active;
	}

	public function getCourse() {
		return $this->course;
	}

	public function setCourse($course) {
		$this->course = $course;
	}

	public function getPrice_privat() {
		return $this->price_privat;
	}

	public function getPrice_escort_in_prague() {
		return $this->price_escort_in_prague;
	}

	public function getPrice_escort_out_prague() {
		return $this->price_escort_out_prague;
	}

	public function getPrice_escort_out_prague_more31() {
		return $this->price_escort_out_prague_more31;
	}

	public function setPrice_privat($price_privat) {
		$this->price_privat = $price_privat;
	}

	public function setPrice_escort_in_prague($price_escort_in_prague) {
		$this->price_escort_in_prague = $price_escort_in_prague;
	}

	public function setPrice_escort_out_prague($price_escort_out_prague) {
		$this->price_escort_out_prague = $price_escort_out_prague;
	}

	public function setPrice_escort_out_prague_more31($price_escort_out_prague_more31) {
		$this->price_escort_out_prague_more31 = $price_escort_out_prague_more31;
	}

	public function getServiceType() {
		return $this->serviceType;
	}

	public function setServiceType($serviceType) {
		$this->serviceType = $serviceType;
	}
	
	public function addServiceType($serviceType){
		$this->serviceType[$serviceType->id] = $serviceType;
	}

}
