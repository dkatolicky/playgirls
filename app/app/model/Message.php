<?php

namespace App\Model;

use Nette;
use Nette\Object;

/**
 * Description of Message
 *
 * @author David
 */
class Message extends Object {

	public $date;
	public $readed;
	public $from;
	public $subject;
	public $text;
	public $id;

	public function getDate() {
		return $this->date;
	}

	public function getReaded() {
		return $this->readed;
	}

	public function getFrom() {
		return $this->from;
	}

	public function getSubject() {
		return $this->subject;
	}

	public function getText() {
		return $this->text;
	}

	public function setDate($date) {
		$this->date = $date;
	}

	public function setReaded($readed) {
		$this->readed = $readed;
	}

	public function setFrom($from) {
		$this->from = $from;
	}

	public function setSubject($subject) {
		$this->subject = $subject;
	}

	public function setText($text) {
		$this->text = $text;
	}

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

}
