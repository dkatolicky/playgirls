<?php

namespace App\Model;

/**
 * Description of Slecna
 *
 * @author David
 */
class Slecna extends BaseModel {

	private $id;
	private $url;
	private $mainfoto;
	private $mainfotoland;
	private $active;
	private $ord;
	private $podnik_id;
	private $podnik;
	private $multiprofil;
	private $multi1;
	private $multi2;
	private $multi3;
	private $multi4;
	private $specialprice;
	private $price_privat;
	private $price_escort_in_prague;
	private $price_escort_out_prague;
	private $price_escort_out_prague_more31;
	private $photo1;
	private $photo2;
	private $photo3;
	private $photo4;
	private $photo5;
	private $langs = array();
	private $serviceTypes = array();
	private $photos = array();

	public function getId() {
		return $this->id;
	}

	public function getUrl() {
		return $this->url;
	}

	public function getMainfoto() {
		return $this->mainfoto;
	}

	public function getActive() {
		return $this->active;
	}

	public function getOrd() {
		return $this->ord;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setUrl($url) {
		$this->url = $url;
	}

	public function setMainfoto($mainfoto) {
		$this->mainfoto = $mainfoto;
	}

	public function setActive($active) {
		$this->active = $active;
	}

	public function setOrd($ord) {
		$this->ord = $ord;
	}

	public function getLangs() {
		return $this->langs;
	}

	public function setLangs($langs) {
		$this->langs = $langs;
	}

	public function addLang($lang, $data) {
		$this->langs[$lang] = $data;
	}

	public function getLang($lang) {
		return $this->langs[$lang];
	}

	public function getServiceTypes() {
		return $this->serviceTypes;
	}

	public function setServiceTypes($serviceTypes) {
		$this->serviceTypes = $serviceTypes;
	}

	public function addService($serviceType) {
		$this->serviceTypes[$service->id] = $service;
	}

	public function getPhotos() {
		return $this->photos;
	}

	public function setPhotos($photos) {
		$this->photos = $photos;
	}

	public function getFirstPhoto() {
		$keys = array_keys($this->photos);
		return $this->photos[$keys[0]];
	}

	public function getPodnik_id() {
		return $this->podnik_id;
	}

	public function getPodnik() {
		return $this->podnik;
	}

	public function setPodnik_id($podnik_id) {
		$this->podnik_id = $podnik_id;
	}

	public function setPodnik($podnik) {
		$this->podnik = $podnik;
	}

	public function getMultiprofil() {
		return $this->multiprofil;
	}

	public function getMulti1() {
		return $this->multi1;
	}

	public function getMulti2() {
		return $this->multi2;
	}

	public function getMulti3() {
		return $this->multi3;
	}

	public function getMulti4() {
		return $this->multi4;
	}

	public function setMultiprofil($multiprofil) {
		$this->multiprofil = $multiprofil;
	}

	public function setMulti1($multi1) {
		$this->multi1 = $multi1;
	}

	public function setMulti2($multi2) {
		$this->multi2 = $multi2;
	}

	public function setMulti3($multi3) {
		$this->multi3 = $multi3;
	}

	public function setMulti4($multi4) {
		$this->multi4 = $multi4;
	}

	public function getSpecialprice() {
		return $this->specialprice;
	}

	public function setSpecialprice($specialprice) {
		$this->specialprice = $specialprice;
	}

	public function getPrice_privat() {
		return $this->price_privat;
	}

	public function getPrice_escort_in_prague() {
		return $this->price_escort_in_prague;
	}

	public function getPrice_escort_out_prague() {
		return $this->price_escort_out_prague;
	}

	public function getPrice_escort_out_prague_more31() {
		return $this->price_escort_out_prague_more31;
	}

	public function setPrice_privat($price_privat) {
		$this->price_privat = $price_privat;
	}

	public function setPrice_escort_in_prague($price_escort_in_prague) {
		$this->price_escort_in_prague = $price_escort_in_prague;
	}

	public function setPrice_escort_out_prague($price_escort_out_prague) {
		$this->price_escort_out_prague = $price_escort_out_prague;
	}

	public function setPrice_escort_out_prague_more31($price_escort_out_prague_more31) {
		$this->price_escort_out_prague_more31 = $price_escort_out_prague_more31;
	}

	public function getMainfotoland() {
		return $this->mainfotoland;
	}

	public function setMainfotoland($mainfotoland) {
		$this->mainfotoland = $mainfotoland;
	}

	public function getPhoto1() {
		return $this->photo1;
	}

	public function getPhoto2() {
		return $this->photo2;
	}

	public function getPhoto3() {
		return $this->photo3;
	}

	public function getPhoto4() {
		return $this->photo4;
	}

	public function getPhoto5() {
		return $this->photo5;
	}

	public function setPhoto1($photo1) {
		$this->photo1 = $photo1;
	}

	public function setPhoto2($photo2) {
		$this->photo2 = $photo2;
	}

	public function setPhoto3($photo3) {
		$this->photo3 = $photo3;
	}

	public function setPhoto4($photo4) {
		$this->photo4 = $photo4;
	}

	public function setPhoto5($photo5) {
		$this->photo5 = $photo5;
	}

	public function getPrices() {
		$ret = new \stdClass();
		$ret->price_privat = new \stdClass();
		$ret->price_escort_in_prague = new \stdClass();
		$ret->price_escort_out_prague = new \stdClass();
		$ret->price_escort_out_prague_more31 = new \stdClass();

		if ($this->podnik instanceof Podnik) {
			foreach (array('price_privat', 'price_escort_in_prague', 'price_escort_out_prague', 'price_escort_out_prague_more31') as $type) {
				for ($i = 0; $i <= 8; $i++) {
					$ret->$type->{'col' . $i} = $this->podnik->$type->{'col' . $i};
				}
			}
		}
		if ($this->specialprice) {
			foreach (array('price_privat', 'price_escort_in_prague', 'price_escort_out_prague', 'price_escort_out_prague_more31') as $type) {
				for ($i = 0; $i <= 8; $i++) {
					if (!empty($this->$type->{'col' . $i})) {
						$ret->$type->{'col' . $i} = $this->$type->{'col' . $i};
					}
				}
			}
		}
		return $ret;
	}

}
