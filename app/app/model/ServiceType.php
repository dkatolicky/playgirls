<?php

namespace App\Model;

/**
 * Description of Slecna
 *
 * @author David
 */
class ServiceType extends BaseModel {

	private $id;
	private $description;
	private $name;
	private $description_en;
	private $name_en;
	private $services = array();

	public function getId() {
		return $this->id;
	}

	public function getDescription() {
		return $this->description;
	}

	public function getDescription_en() {
		return $this->description_en;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function setDescription_en($description_en) {
		$this->description_en = $description_en;
	}

	public function getServices() {
		return $this->services;
	}

	public function setServices($services) {
		$this->services = $services;
	}

	public function getName() {
		return $this->name;
	}

	public function getName_en() {
		return $this->name_en;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function setName_en($name_en) {
		$this->name_en = $name_en;
	}

}
