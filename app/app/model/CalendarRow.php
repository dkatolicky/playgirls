<?php

namespace App\Model;

use Nette;

/**
 * Description of CalendarRow
 *
 * @author Pali Bazant <pali.bazant@gmail.com>
 */
class CalendarRow extends BaseModel {

	private $id;
	private $slecna_id;
	private $date;
	private $muze_od_do;
	private $online = 0;
	private $cely_den_online_note = '';
	private $na_dotaz_zavolanim = 0;
	private $na_dotaz_pres_sms = 0;
	private $muze_od = '';
	private $muze_do = '';
	private $cas_dojezdu_ke_klientovi_note = '';
	private $cas_dojezdu_do_prahy_note = '';
	private $divka_je_v_dany_den_v = '';
	private $note = '';

	public function getOnline() {
		if ($this->online == 0) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function setOnline($online) {
		$this->online = $online;
	}

	public function getCely_den_online_note() {
		if ($this->cely_den_online_note == '') {
			return NULL;
		} else {
			return $this->cely_den_online_note;
		}
	}

	public function setCely_den_online_note($cely_den_online_note) {
		$this->cely_den_online_note = $cely_den_online_note;
	}

	public function getNa_dotaz_zavolanim() {
		if ($this->na_dotaz_zavolanim == 0) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function setNa_dotaz_zavolanim($na_dotaz_zavolanim) {
		$this->na_dotaz_zavolanim = $na_dotaz_zavolanim;
	}

	public function getNa_dotaz_pres_sms() {
		if ($this->na_dotaz_pres_sms == 0) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function setNa_dotaz_pres_sms($na_dotaz_pres_sms) {
		$this->na_dotaz_pres_sms = $na_dotaz_pres_sms;
	}

	public function getMuze_od() {
		if ($this->muze_od == '') {
			return NULL;
		} else {
			return $this->muze_od;
		}
	}

	public function setMuze_od($muze_od) {
		$this->muze_od = $muze_od;
	}

	public function getMuze_do() {
		if ($this->muze_do == '') {
			return NULL;
		} else {
			return $this->muze_do;
		}
	}

	public function setMuze_do($muze_do) {
		$this->muze_do = $muze_do;
	}

	public function getCas_dojezdu_ke_klientovi_note() {
		if ($this->cas_dojezdu_ke_klientovi_note == '') {
			return NULL;
		} else {
			return $this->cas_dojezdu_ke_klientovi_note;
		}
	}

	public function setCas_dojezdu_ke_klientovi_note($cas_dojezdu_ke_klientovi_note) {
		$this->cas_dojezdu_ke_klientovi_note = $cas_dojezdu_ke_klientovi_note;
	}

	public function getCas_dojezdu_do_prahy_note() {
		if ($this->cas_dojezdu_do_prahy_note == '') {
			return NULL;
		} else {
			return $this->cas_dojezdu_do_prahy_note;
		}
	}

	public function setCas_dojezdu_do_prahy_note($cas_dojezdu_do_prahy_note) {
		$this->cas_dojezdu_do_prahy_note = $cas_dojezdu_do_prahy_note;
	}

	public function getDivka_je_v_dany_den_v() {
		if ($this->divka_je_v_dany_den_v == '') {
			return NULL;
		} else {
			return $this->divka_je_v_dany_den_v;
		}
	}

	public function setDivka_je_v_dany_den_v($divka_je_v_dany_den_v) {
		$this->divka_je_v_dany_den_v = $divka_je_v_dany_den_v;
	}

	public function getNote() {
		if ($this->note == '') {
			return NULL;
		} else {
			return $this->note;
		}
	}

	public function setNote($note) {
		$this->note = $note;
	}

	public function callName($name) {
		return $this->$name;
	}

	public function getId() {
		return $this->id;
	}

	public function getSlecna_id() {
		return $this->slecna_id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setSlecna_id($slecna_id) {
		$this->slecna_id = $slecna_id;
	}

	public function getDate() {
		return $this->date;
	}

	public function setDate($date) {
		$this->date = $date;
	}

	public function getMuze_od_do() {
		return $this->muze_od_do;
	}

	public function setMuze_od_do($muze_od_do) {
		$this->muze_od_do = $muze_od_do;
	}
	
	public function isAllDayOnline(){
		if($this->online == 1){
			return true;
		}
		return false;
	}
	public function isOnlineOnRequest(){
		if(!empty($this->muze_od) || !empty($this->muze_do)){
			return true;
		}
		return false;
	}

}

?>
