<?php //netteCache[01]000399a:2:{s:4:"time";s:21:"0.44365900 1429094890";s:9:"callbacks";a:2:{i:0;a:3:{i:0;a:2:{i:0;s:19:"Nette\Caching\Cache";i:1;s:9:"checkFile";}i:1;s:85:"/home/www/nycco/git/zacatecnice/app/app/templates/Pages/forms/reservation.email.latte";i:2;i:1429007240;}i:1;a:3:{i:0;a:2:{i:0;s:19:"Nette\Caching\Cache";i:1;s:10:"checkConst";}i:1;s:25:"Nette\Framework::REVISION";i:2;s:22:"released on 2014-05-24";}}}?><?php

// source file: /home/www/nycco/git/zacatecnice/app/app/templates/Pages/forms/reservation.email.latte

?><?php
// prolog Nette\Latte\Macros\CoreMacros
list($_l, $_g) = Nette\Latte\Macros\CoreMacros::initRuntime($template, '0d7npncbt5')
;
// prolog Nette\Latte\Macros\UIMacros

// snippets support
if (!empty($_control->snippetMode)) {
	return Nette\Latte\Macros\UIMacros::renderSnippets($_control, $_l, get_defined_vars());
}

//
// main template
//
?>
<p>Email z formuláře rezervace</p>
<table>
	<tr>
		<td>Jméno, kterým Tě máme oslovovat:</td>
		<td><?php echo Nette\Templating\Helpers::escapeHtml($values->jmeno, ENT_NOQUOTES) ?></td>
	</tr>
	<tr>
		<td>Tvůj kontaktní telefon:</td>
		<td><?php echo Nette\Templating\Helpers::escapeHtml($values->tel, ENT_NOQUOTES) ?></td>
	</tr>
	<tr>
		<td>Tvůj kontaktní e-mail:</td>
		<td><?php echo Nette\Templating\Helpers::escapeHtml($values->email, ENT_NOQUOTES) ?></td>
	</tr>
	<tr>
		<td>Datum, kdy máš zájem o jednu z nás:</td>
		<td><?php echo Nette\Templating\Helpers::escapeHtml($values->datum, ENT_NOQUOTES) ?></td>
	</tr>
	<tr>
		<td>Čas, kdy máme přijet:</td>
		<td><?php echo Nette\Templating\Helpers::escapeHtml($values->prijezd, ENT_NOQUOTES) ?></td>
	</tr>
	<tr>
		<td>Předpoklad délky společného času:</td>
		<td><?php echo Nette\Templating\Helpers::escapeHtml($values->delka, ENT_NOQUOTES) ?></td>
	</tr>
	<tr>
		<td>Preferovaná slečna, kterou by jsi nejraději:</td>
		<td><?php echo Nette\Templating\Helpers::escapeHtml($values->preferSlecna, ENT_NOQUOTES) ?></td>
	</tr>
	<tr>
		<td>Pokud v uvedeném čase nebude slečna moct, uveď prosím jméno jiné preferované slečny/slečen:</td>
		<td><?php echo Nette\Templating\Helpers::escapeHtml($values->preferSlecna2, ENT_NOQUOTES) ?></td>
	</tr>
	<tr>
		<td>Adresa, kam máme přijet:</td>
		<td><?php echo Nette\Templating\Helpers::escapeHtml($values->adresa, ENT_NOQUOTES) ?></td>
	</tr>
	<tr>
		<td>Je to:</td>
		<td><?php echo Nette\Templating\Helpers::escapeHtml($values->houseType, ENT_NOQUOTES) ?></td>
	</tr>
	<tr>
		<td>Doplňující poznámky a požadavky:</td>
		<td><?php echo Nette\Templating\Helpers::escapeHtml($values->poznamky, ENT_NOQUOTES) ?></td>
	</tr>
</table>